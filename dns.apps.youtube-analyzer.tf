resource "cloudflare_record" "youtube-analyser" {
  name       = "youtube-analyser"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}