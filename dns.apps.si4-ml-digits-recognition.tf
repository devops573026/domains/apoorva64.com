resource "cloudflare_record" "si4-ml-digits-recognition-prod-cname" {
  name       = "si4-ml-digits-recognition"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}
