// create a dns CNAME record to loadbalancer.apoorva64.com that depends on the load balancer

resource "cloudflare_record" "react-flight-tracker-prod-cname" {
  name       = "react-flight-tracker"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}

resource "cloudflare_record" "react-flight-tracker-dev-cname" {
  name       = "react-flight-tracker.dev"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}


resource "cloudflare_record" "satellite-api-react-flight-tracker-prod-cname" {
  name       = "satellite-api.react-flight-tracker"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}

resource "cloudflare_record" "satellite-api-react-flight-tracker-dev-cname" {
  name       = "satellite-api.react-flight-tracker.dev"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}