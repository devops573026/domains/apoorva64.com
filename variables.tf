variable "cloudflare_api_token" {
  type        = string
  description = "Cloudflare api token"
  sensitive   = true
}

variable "cloudflare_zone_id" {
  type        = string
  description = "Cloudflare zone id"
  sensitive   = true
}

variable "domain" {
  type        = string
  description = "Domain name"
  default     = "apoorva64.com"
}