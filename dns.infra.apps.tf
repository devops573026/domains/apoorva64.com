resource "cloudflare_record" "keycloak-prod-cname" {
  name       = "keycloak.auth"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}

resource "cloudflare_record" "weave-gitops-prod-cname" {
  name       = "weave-gitops.devops-tools"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}


resource "cloudflare_record" "grafana-prod-cname" {
  name       = "grafana.monitoring"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}

resource "cloudflare_record" "minio-prod-cname" {
  name       = "minio.storage"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}

resource "cloudflare_record" "api-minio-prod-cname" {
  name       = "api.minio.storage"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}


resource "cloudflare_record" "sonarqube-prod-cname" {
  name       = "sonarqube.devops-tools"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}

resource "cloudflare_record" "minecraft-prod-cname" {
  name       = "minecraft.games"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.a1_dns.hostname
  depends_on = [cloudflare_record.a1_dns]
}

resource "cloudflare_record" "ray-serve-prod-cname" {
  name       = "*.ray"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}

resource "cloudflare_record" "oauth2-proxy-prod-cname" {
  name       = "oauth2-proxy.auth"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}

resource "cloudflare_record" "kibana-prod-cname" {
  name       = "kibana.monitoring"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}

resource "cloudflare_record" "longhorn-prod-cname" {
  name       = "longhorn.storage"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}

resource "cloudflare_record" "open-webui-ai-prod-cname" {
  name       = "open-webui.ai"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}

resource "cloudflare_record" "ai-prod-cname" {
  name       = "*.ai"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}

resource "cloudflare_record" "harbor-devops-tools-prod-cname" {
  name       = "harbor.devops-tools"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}

resource "cloudflare_record" "minio-main-prod-cname" {
  name       = "minio-main.storage"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}

resource "cloudflare_record" "api-minio-main-prod-cname" {
  name       = "api.minio-main.storage"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}