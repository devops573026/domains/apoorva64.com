resource "cloudflare_record" "www" {
  name    = "www"
  type    = "CNAME"
  value   = var.domain
  zone_id = var.cloudflare_zone_id
}