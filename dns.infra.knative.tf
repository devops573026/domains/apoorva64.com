# Add knative dns records
resource "cloudflare_record" "knative" {
  name    = "knative"
  type    = "CNAME"
  zone_id = var.cloudflare_zone_id
  value   = cloudflare_record.istio_load_balancer_dns.hostname

}

# Add knative dns records
resource "cloudflare_record" "default-knative" {
  name    = "*.knative"
  type    = "CNAME"
  zone_id = var.cloudflare_zone_id
  value   = cloudflare_record.istio_load_balancer_dns.hostname
}