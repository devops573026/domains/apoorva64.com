# Add a record to arm_cluster node 1
resource "cloudflare_record" "a1_dns" {
  zone_id = var.cloudflare_zone_id
  name    = "a1"
  value   = "158.101.222.29"
  type    = "A"
  ttl     = 1
  comment = "a1 node in arm-cluster"
}

# Add a record to arm_cluster node 2 (node_apoorva)
resource "cloudflare_record" "node_apoorva_dns" {
  name    = "node-apoorva"
  type    = "A"
  zone_id = var.cloudflare_zone_id
  value   = "158.178.207.158"
  ttl     = 1
  comment = "node-apoorva node in arm-cluster"
}

# Add a record to arm_cluster node 3 (node_raphael)
resource "cloudflare_record" "node_raphael_dns" {
  name    = "node-raphael"
  type    = "A"
  zone_id = var.cloudflare_zone_id
  value   = "129.151.254.9"
  ttl     = 1
  comment = "node-raphael node in arm-cluster"
}

# Add a record to arm_cluster node 4 (node_tom)
resource "cloudflare_record" "node_tom_dns" {
  name    = "node-tom"
  type    = "A"
  zone_id = var.cloudflare_zone_id
  value   = "129.151.255.181"
  ttl     = 1
  comment = "node-tom node in arm-cluster"
}

# Add a record to arm_cluster node 5 (node_raphael2)
resource "cloudflare_record" "node_raphael2_dns" {
  name    = "node-raphael2"
  type    = "A"
  zone_id = var.cloudflare_zone_id
  value   = "204.216.214.156"
  ttl     = 1
  comment = "node-raphael2 node in arm-cluster"
}

# Add a record to arm_cluster node 3
resource "cloudflare_record" "a3_dns" {
  name    = "a3"
  type    = "A"
  zone_id = var.cloudflare_zone_id
  value   = "158.178.203.160"
  ttl     = 1
  comment = "a3 node in arm-cluster"
}


# Add load balancer record
resource "cloudflare_record" "arm_load_balancer_dns" {
  name    = "loadbalancer"
  type    = "A"
  zone_id = var.cloudflare_zone_id
  value   = "129.151.229.226"
  ttl     = 1
  comment = "load balancer in arm-cluster"
}

# Add Headscale record
resource "cloudflare_record" "headscale_dns" {
  name       = "headscale"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}

# Add Istio load balancer record
resource "cloudflare_record" "istio_load_balancer_dns" {
  name    = "k3s-loadbalancer-istio"
  type    = "A"
  zone_id = var.cloudflare_zone_id
  value   = "141.145.215.199"
  ttl     = 1
  comment = "istio load balancer in arm-cluster"
}

