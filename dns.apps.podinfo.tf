// create a dns CNAME record to loadbalancer.apoorva64.com that depends on the load balancer

resource "cloudflare_record" "pod-info-prod-cname" {
  name       = "pod-info"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}

resource "cloudflare_record" "pod-info-staging-cname" {
  name       = "staging.pod-info"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}