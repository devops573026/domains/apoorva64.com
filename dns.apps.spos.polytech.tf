resource "cloudflare_record" "spos-polytech-cname" {
  name       = "*.spos.polytech"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}

resource "cloudflare_record" "spos-dev-polytech-cname" {
  name       = "*.spos.dev.polytech"
  type       = "CNAME"
  zone_id    = var.cloudflare_zone_id
  value      = cloudflare_record.arm_load_balancer_dns.hostname
  depends_on = [cloudflare_record.arm_load_balancer_dns]
}